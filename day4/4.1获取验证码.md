在src/views/user/Login.vue文件中
```vue
<template>
  <div class="login-page">
    <el-row>
      <el-col class="title">
        <h2>欢迎登录</h2>
      </el-col>
    </el-row>
    <el-row>
      <el-col :span="10" :offset="7">
        <el-form
          :model="ruleForm"
          status-icon
          :rules="rules"
          ref="ruleForm"
          label-width="100px"
          class="demo-ruleForm"
        >
          <el-form-item label="请输入邮箱" prop="username">
            <el-input
              type="text"
              placeholder="请输入邮箱"
              v-model="ruleForm.username"
              autocomplete="off"
            ></el-input>
          </el-form-item>
          <el-form-item label="请输入密码" prop="password">
            <el-input
              type="password"
              placeholder="请输入密码"
              v-model="ruleForm.password"
              autocomplete="off"
            ></el-input>
          </el-form-item>
          <el-form-item label="验证码" prop="code">
            <el-row>
              <el-col :span="6">
                <el-input type="text" placeholder="验证码" v-model="ruleForm.code" autocomplete="off">验证码</el-input>
              </el-col>
              <el-col :span="6" :offset="1">
                <div>验证码</div>
              </el-col>
              <el-col :span="5" :offset="6">
                <el-button type="text" @click="$router.push('/forget')">忘记密码？</el-button>
              </el-col>
            </el-row>
          </el-form-item>
          <el-form-item>
            <el-button type="primary" @click="submitForm('ruleForm')">登录</el-button>
            <el-button @click="resetForm('ruleForm')">重置</el-button>
          </el-form-item>
        </el-form>
      </el-col>
    </el-row>
  </div>
</template>

<script>
export default {
  data() {
    var checkEmail = (rule, value, callback) => {
      if (!value) {
        return callback(new Error("用户名不正确"));
      } else {
        callback();
      }
    };
    var checkPassword = (rule, value, callback) => {
      if (!value) {
        return callback(new Error("请输入密码"));
      } else {
        callback();
      }
    };
    var checkCode = (rule, value, callback) => {
      if (value.length != 4) {
        return callback(new Error("验证码长度必须为4"));
      } else {
        callback();
      }
    };
    return {
      ruleForm: {
        username: "",
        password: "",
        code: ""
      },
      rules: {
        username: [{ validator: checkEmail, trigger: "blur" }],
        password: [{ validator: checkPassword, trigger: "blur" }],
        code: [{ validator: checkCode, trigger: "blur" }]
      },
      verify: ""
    };
  },
  methods: {
    submitForm(formName) {
      this.$refs[formName].validate(async valid => {
        if (valid) {
          alert(1);//登陆成功后返回结果
          console.log(this.ruleForm)
        } else {
          return false;
        }
      });
    },
    resetForm(formName) {
      this.$refs[formName].resetFields();
    },
  },
};
</script>


<style lang="scss">
.title {
  text-align: center;
  line-height: 50px;
}
</style>
```
**获取验证码的接口:**[**http://vue.zhufengpeixun.cn**](http://vue.zhufengpeixun.cn/public/getSlider)**/public/getCaptcha**<br />src/api/config/public.js文件----配置文件
```javascript
export default {
    getSlider: '/public/getSlider',  // 获取轮播图的接口
    getCaptcha:'/public/getCaptcha',//获取验证码的接口
}
```
src/api/public文件 ---- 接口处理文件
```javascript
import config from './config/public';
import axios from '@/utils/request'
import { getLocal } from '@/utils/local.js' //新建local.js文件存储本地会话

// 获取轮播图功能
export const getSlider = () => axios.get(config.getSlider);

// 创建一个唯一的用户标识 和  验证码对应上  1：1234
export const getCaptcha = () => axios.get(config.getCaptcha, {
  //通过获取本地会话可以得到不同的uid的值，也就是获取不同的验证码
    params: {
        uid: getLocal('uuid')
    }
})
```
在src/utils文件下新建local.js文件
```javascript
// 设置的时候需要将 对象转成字符串
export const setLocal = (key,value)=>{
    if(typeof value === 'object'){
        value = JSON.stringify(value)
    }
    localStorage.setItem(key,value);
}

// 获取本地的值 需要转化成对象
export const getLocal = (key)=>{
    let value = localStorage.getItem(key) || ''
    if(value.includes('[') || value.includes('{')){
        return JSON.parse(value);
    }else{
        return localStorage.getItem(key) || '';
    }
}

//挂载到window上
window.setLocal = setLocal;
window.getLocal = getLocal
```
在登录页要进行存储本地会话，产生唯一的标识<br />src/views/user/Login.vue文件
```vue
<template>
//...
    <el-row>
        <el-col :span="6">
          <el-input type="text" placeholder="验证码" v-model="ruleForm.code" autocomplete="off">验证码</el-input>
        </el-col>
        <el-col :span="6" :offset="1">
          <div v-html="verify" @click="getCaptcha"></div>
        </el-col>
        <el-col :span="5" :offset="6">
          <el-button type="text" @click="$router.push('/forget')">忘记密码？</el-button>
         </el-col>
      </el-row>
//...
</template>

<script>
import { v4 } from "uuid";//uuid中有v4方法，产生不同的字符串
import { getLocal, setLocal } from "@/utils/local.js";
import { getCaptcha } from "@/api/public";
export default {
 //...
  methods: {
//...
    resetForm(formName) {
      this.$refs[formName].resetFields();
    },
    async getCaptcha(){
        let {data} = await getCaptcha();
        //console.log(data)
        this.verify=data;
    },
  }
  async created() {
    this.uid = getLocal("uuid");
    if (!this.uid) {//如果没有uid的话就会重新获取一个
      setLocal("uuid", v4());
    }
    this.getCaptcha();//每一次点击就刷新一次验证码
  }
};
</script>
```